package net.ihe.gazelle.simulators.authentication.adapter;


import net.ihe.gazelle.simulators.authentication.application.IdPSimulator;
import net.ihe.gazelle.simulators.authentication.application.IdpException;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Stateless
@Name("transformationWS")
@WebService(
        name = "IdPSimulatorWS",
        targetNamespace = "http://ws.authentication.simulators.gazelle.ihe.net/",
        serviceName = "IdPSimulatorService"
)
@HandlerChain(file = "/META-INF/soap-handlers.xml")
public class ShibbolethIdPSimulator implements IdPSimulator {

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethIdPSimulator.class);

    private static final String IDP_METADATA_FILE_PATH = "/opt/shibboleth-idp/metadata/idp-metadata.xml";
    private static final String METADATA_RESOLVER_SERVICE = "shibboleth.MetadataResolverService";
    private static final String SHIBBOLETH_RESTART_COMMAND = "sudo service tomcat8 restart";
    private static final String SHIBBOLETH_RELOAD_SERVICE_COMMAND = "/opt/shibboleth-idp/bin/reload-service.sh";

    @In
    private AttemptBuffer attemptBuffer;

    @Override
    public byte[] getIdPSimuMedatada() throws IdpException {
        try {
            return Files.readAllBytes(Paths.get(IDP_METADATA_FILE_PATH));
        } catch (IOException e) {
            LOG.error("Failed to read idp metadata file", e);
            throw new IdpException("Failed to read idp metadata file", e);
        }
    }

    @Override
    public List<AttemptRecordedByIdp> fetchAuthenticationAttempts() throws IdpException {
        List<Exception> exceptions = attemptBuffer.getExceptions();
        if (exceptions != null && !exceptions.isEmpty()) {
            throw new IdpException("Errors reported while capturing IdP logs", exceptions);
        } else {
            return attemptBuffer.getAttemptsRecordedByIdp();
        }
    }

    @Override
    public void reloadIdPMetadata() throws IdpException {
        try {
            reloadIdPServices(METADATA_RESOLVER_SERVICE);
        } catch (IdpException e) {
            LOG.error("Failed to reload Metadata: {}", e.getMessage());
            throw e;
        }
    }

    @Override
    public void rebootIdp() throws IdpException {
        try {
            //FIXME watch return process to verify command execution
            Runtime.getRuntime().exec(new String[]{SHIBBOLETH_RESTART_COMMAND});
        } catch (IOException e) {
            LOG.error("Failed to reboot Shibboleth IdP", e);
            throw new IdpException("Failed to reboot Shibboleth IdP", e);
        }
    }

    private void reloadIdPServices(String serviceName) throws IdpException {
        try {
            //FIXME watch return process to verify command execution
            Runtime.getRuntime().exec(new String[]{SHIBBOLETH_RELOAD_SERVICE_COMMAND, "-id", serviceName});
        } catch (IOException e) {
            throw new IdpException("Failed to relaod service " + serviceName, e);
        }
    }

}
