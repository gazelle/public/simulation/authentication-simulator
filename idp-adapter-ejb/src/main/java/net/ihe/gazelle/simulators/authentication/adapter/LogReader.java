package net.ihe.gazelle.simulators.authentication.adapter;

import org.apache.commons.io.IOUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

@Name("logReader")
@Scope(ScopeType.APPLICATION)
public class LogReader implements Serializable {

    private static final long serialVersionUID = 743130390833526236L;
    private static final Logger LOG = LoggerFactory.getLogger(LogReader.class);

    public static final String READ_LOG_EVENT = "net.ihe.gazelle.simulators.authentication.ReadLogEvent";

    private BufferedReader logBufferedReader = null;
    private long logFileLastModifiedAtLastRead = 0L;

    @In
    private LogParser logParser;

    @Observer(READ_LOG_EVENT)
    public synchronized void readLog(String filePath) {
        LOG.debug("readLog triggered...");
        try {
            if (logBufferedReader == null) {
                logBufferedReader = Files.newBufferedReader(Paths.get(filePath), StandardCharsets.UTF_8);
            }

            long logFileLastModified = Files.getLastModifiedTime(Paths.get(filePath)).toMillis();

            String line = null;
            while ((line = logBufferedReader.readLine()) != null) {
                LOG.debug("Read log line: {}...", line.substring(0, 35));
                logParser.processLogLine(line);
                logFileLastModifiedAtLastRead = logFileLastModified;
            }

            if (logFileLastModified != logFileLastModifiedAtLastRead) {
                destroy();
            }
        } catch (SecurityException | IOException e) {
            LOG.error("Error while accessing/reading log file " + filePath, e);
            destroy();
            throw new LogReaderException("Unable to access/read log file" + filePath, e);
        }
        LOG.debug("readLog end");
    }

    public void handleAsynchronousException(Exception exception) {
        logParser.forwardException(exception);
    }

    @Destroy
    public synchronized void destroy() {
        if (logBufferedReader != null) {
            IOUtils.closeQuietly(logBufferedReader);
            logBufferedReader = null;
            logFileLastModifiedAtLastRead = 0L;
        }
    }

    boolean isLogBufferedReaderClosedAndNull() {
        return logBufferedReader == null;
    }

}
