package net.ihe.gazelle.simulators.authentication.adapter;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.BindingType;
import net.ihe.gazelle.simulators.authentication.model.LocationType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@AutoCreate
@Name("logParser")
public class ShibbolethIdPAuditLogParser implements LogParser {

    // https://wiki.shibboleth.net/confluence/display/IDP30/AuditLoggingConfiguration
    // There are initially two such logs provided, and there might be more added later if other use cases emerge. The core audit log is used for
    // general request/response auditing, and is routed by default through a logger named "Shibboleth-Audit" to a file called idp-audit.log. A
    // second log is used for logging decisions made by users over attribute release and terms of use acceptance, and is routed by default
    // through a logger named "Shibboleth-Consent-Audit" to a file called idp-consent-audit.log.

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethIdPAuditLogParser.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    private static final String LOG_REGEXP_DELIMITER = "\\|";

    private static final int TIMESTAMP_INDEX = 0;
    private static final int INBOUND_BINDING_INDEX = 1;
    private static final int INBOUND_MESSAGE_ID_INDEX = 2;
    private static final int SERVICE_PROVIDER_NAME_INDEX = 3;
    private static final int PROFILE_ID_INDEX = 4;
    private static final int IDENDITY_PROVIDER_NAME_INDEX = 5;
    private static final int OUTBOUND_BINDING_INDEX = 6;
    private static final int OUTBOUND_MESSAGE_ID_INDEX = 7;
    private static final int USERNAME_INDEX = 8;
    private static final int AUTH_CONTEXT_INDEX = 9;
    private static final int ATTRIBUTES_INDEX = 10;
    private static final int NAME_ID_INDEX = 11;
    private static final int ASSERTION_ID_INDEX = 12;
    private static final int URL = 13;
    private static final int STATUS = 14;

    @In
    private AttemptBuffer attemptBuffer;

    @Override
    public void processLogLine(String logLine) {
        AttemptRecordedByIdp record = extractValues(logLine);
        if (attemptBuffer.getLastRecordDate() == null || (record.getTimestamp() != null && record.getTimestamp()
                .after(attemptBuffer.getLastRecordDate()))) {
            attemptBuffer.add(record);
        }
    }

    @Override
    public void forwardException(Exception e) {
        attemptBuffer.addException(e);
    }

    private AttemptRecordedByIdp extractValues(String line) {

        Date timestamp = null;
        String username = null;
        BindingType usedBinding = null;
        String assertionId = null;
        String serviceProviderName = null;
        String identityProviderName = null;
        boolean success = false;
        String status = null;
        LocationType location = null;

        String[] tokens = line.split(LOG_REGEXP_DELIMITER);

        timestamp = convertStringToDate(getTokenValueOrNull(tokens, TIMESTAMP_INDEX));
        usedBinding = convertStringBindingType(getTokenValueOrNull(tokens, INBOUND_BINDING_INDEX));
        location =  convertStringLocationType(getTokenValueOrNull(tokens, URL));
        serviceProviderName = getTokenValueOrNull(tokens, SERVICE_PROVIDER_NAME_INDEX);
        identityProviderName = getTokenValueOrNull(tokens, IDENDITY_PROVIDER_NAME_INDEX);
        username = getTokenValueOrNull(tokens, USERNAME_INDEX);
        assertionId = getTokenValueOrNull(tokens, ASSERTION_ID_INDEX);
        status = getTokenValueOrNull(tokens, STATUS);
        success = computeSuccess(status);

        return new AttemptRecordedByIdp(timestamp, username, serviceProviderName, identityProviderName, success,
                usedBinding, assertionId, location);

    }

    private Date convertStringToDate(String date) {
        // 2018-07-13T14:47:28.061+02:00
        if (date != null && !date.isEmpty()) {
            try {
                return new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH).parse(date);
            } catch (ParseException e) {
                LOG.warn("Failed to parse log date '{}': {}", date, e.getMessage());
            }
        }
        return null;
    }

    private BindingType convertStringBindingType(String inboundBinding) {
        if (inboundBinding != null && !inboundBinding.isEmpty()) {
            return BindingType.getBindingTypeByLabel(inboundBinding);
        }
        return null;
    }

    private LocationType convertStringLocationType(String url) {
        if (url != null && !url.isEmpty()) {
            return LocationType.getLocationTypeByUrl(url);
        }
        return null;
    }

    private String getTokenValueOrNull(String[] tokens, int tokenIndex) {
        return tokens.length > tokenIndex ? tokens[tokenIndex] : null;
    }

    private boolean computeSuccess(String status) {
        if (status != null && status.equals("urn:oasis:names:tc:SAML:2.0:status:Success")){
            return true;
        } else {
            return false;
        }
    }
}
