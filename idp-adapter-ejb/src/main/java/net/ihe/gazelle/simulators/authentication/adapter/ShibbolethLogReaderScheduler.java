package net.ihe.gazelle.simulators.authentication.adapter;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.async.QuartzTriggerHandle;
import org.jboss.seam.async.TimerSchedule;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Scope(ScopeType.APPLICATION)
@Name("logReaderScheduler")
public class ShibbolethLogReaderScheduler {

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethLogReaderScheduler.class);

    private static final String SHIBBOLETH_AUDIT_LOG_PATH = "/opt/shibboleth-idp/logs/idp-audit.log";
    private static final String JBOSS_SEAM_POST_INIT_EVENT = "org.jboss.seam.postInitialization";
    private static final long READ_INTERVAL_MS = 3 * 1000L;

    private QuartzTriggerHandle triggerHandle;

    @Observer(value = JBOSS_SEAM_POST_INIT_EVENT)
    public void startReader() {
        triggerHandle = QuartzDispatcher.instance()
                .scheduleTimedEvent(LogReader.READ_LOG_EVENT, new TimerSchedule(READ_INTERVAL_MS, READ_INTERVAL_MS), SHIBBOLETH_AUDIT_LOG_PATH);
        LOG.info("Event {} scheduled every {} ms", LogReader.READ_LOG_EVENT, READ_INTERVAL_MS);
    }

    @Destroy
    public void destroy() {
        if (triggerHandle != null) {
            try {
                triggerHandle.cancel();
                LOG.info("Event {} canceled", LogReader.READ_LOG_EVENT);
            } catch (SchedulerException e) {
                LOG.error("Unable to cancel {} event schedule", LogReader.READ_LOG_EVENT);
            }
        }
    }

}
