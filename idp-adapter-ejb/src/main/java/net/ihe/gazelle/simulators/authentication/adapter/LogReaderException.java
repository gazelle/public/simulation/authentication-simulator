package net.ihe.gazelle.simulators.authentication.adapter;

public class LogReaderException extends RuntimeException {

    private static final long serialVersionUID = 4267895354148708167L;

    public LogReaderException() {
        super();
    }

    public LogReaderException(String s) {
        super(s);
    }

    public LogReaderException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public LogReaderException(Throwable throwable) {
        super(throwable);
    }

    protected LogReaderException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
