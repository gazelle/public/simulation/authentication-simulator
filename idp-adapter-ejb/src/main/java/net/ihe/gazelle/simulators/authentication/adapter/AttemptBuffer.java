package net.ihe.gazelle.simulators.authentication.adapter;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Scope(ScopeType.APPLICATION)
@Name("attemptBuffer")
@AutoCreate
public class AttemptBuffer {

    private static final Logger LOG = LoggerFactory.getLogger(AttemptBuffer.class);

    private Date lastRecordDate = null;
    private List<AttemptRecordedByIdp> attemptRecordedByIdps = new ArrayList<>();
    private List<Exception> exceptions = new ArrayList<>();


    public synchronized List<AttemptRecordedByIdp> getAttemptsRecordedByIdp() {
        List<AttemptRecordedByIdp> attemptRecordedByIdpsClone = new ArrayList<>(attemptRecordedByIdps);
        attemptRecordedByIdps.clear();
        LOG.debug("Records fetched from buffer and cleared");
        return attemptRecordedByIdpsClone;
    }

    public synchronized void add(AttemptRecordedByIdp record) {
        attemptRecordedByIdps.add(record);
        lastRecordDate = (Date) record.getTimestamp().clone();
        LOG.info("Record added in buffer {}, {}...", lastRecordDate, record.getUsername());
    }

    public synchronized List<Exception> getExceptions() {
        if (!exceptions.isEmpty()) {
            List<Exception> exceptionsClone = new ArrayList<>(exceptions);
            exceptions.clear();
            LOG.warn("Exceptions fetched from buffer and cleared");
            return exceptionsClone;
        } else {
            return new ArrayList<>();
        }
    }

    public synchronized void addException(Exception e) {
        exceptions.add(e);
    }

    public synchronized Date getLastRecordDate() {
        return lastRecordDate == null ? null : (Date) lastRecordDate.clone();
    }

}
