package net.ihe.gazelle.simulators.authentication.adapter;

public interface LogParser {

    void processLogLine(String logLine);

    void forwardException(Exception e);

}
