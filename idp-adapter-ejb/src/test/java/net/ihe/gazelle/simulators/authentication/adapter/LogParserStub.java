package net.ihe.gazelle.simulators.authentication.adapter;

import java.util.ArrayList;
import java.util.List;

public class LogParserStub implements LogParser {

    private List<String> logLines = new ArrayList<>();
    private List<Exception> exceptions = new ArrayList<>();

    @Override
    public void processLogLine(String logLine) {
        logLines.add(logLine);
    }

    @Override
    public void forwardException(Exception e) {
        exceptions.add(e);
    }

    public List<String> getLogLines() {
        return logLines;
    }

    public List<Exception> getExceptions() {
        return exceptions;
    }
}
