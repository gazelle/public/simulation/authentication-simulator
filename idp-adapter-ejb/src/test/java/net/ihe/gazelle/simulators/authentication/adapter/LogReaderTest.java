package net.ihe.gazelle.simulators.authentication.adapter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

@RunWith(MockitoJUnitRunner.class)
public class LogReaderTest {

    private static final int LOG_ENTRY_NUMBER = 24;
    private static final String VALID_HTTP_LOG_LINE = "2018-07-13T15:12:15.374+02:00|urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" +
            "|_09deb95b86993a210a3ff526644a6daf|https" +
            "://sp.ihe-europe.net/shibboleth|http://shibboleth.net/ns/profiles/saml2/sso/browser|https://idp.ihe-europe" +
            ".net/idp/shibboleth|urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST|_6908c0278a0bdd509533a9bc57571b36|gthomazon|urn:oasis:names:tc" +
            ":SAML:2.0:ac:classes:PasswordProtectedTransport|lastName,uid,gender,displayName,givenName," +
            "dateOfBirth|AAdzZWNyZXQxcv/GFd8iA7fF4e/B/LQljORW81c/PJKA4djobfZ9lEsKS9FutxH6yIoqky3O/PDaP5VMsBmp8cLAMZX0" +
            "+8XOXoSWJuaoX4cWL9x6GfhmWAuPHKrnBZfvTKhnaB3LFbLjmEWkhXvaCC8ezWd4|_6cfb093123c2416512c8ce9c49c12f55|";

    @Spy
    LogParserStub logParser = new LogParserStub();

    @InjectMocks
    LogReader logReader = new LogReader();

    private File auditTmpFile = null;

    @Before
    public void setUp() throws IOException {
        auditTmpFile = File.createTempFile("idp-audit", ".log");
        copyFiles(new File("src/test/resources/idp-audit.log"), auditTmpFile);
    }

    @Test
    public void readLogTest() {
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must parse the log file entirely at first call", LOG_ENTRY_NUMBER, logParser.getLogLines().size());
    }

    @Test
    public void readLogNoFileTest() {
        try {
            logReader.readLog("src/test/resources/missing-file.log");
            Assert.fail("logReader must throw an exception in case of unaccessible file");
        } catch (LogReaderException e) {
            Assert.assertTrue(
                    "When an exception occurs, LogReader must close and nullify its logBufferedReader. To be able to retry later",
                    logReader.isLogBufferedReaderClosedAndNull());
        }
    }

    @Test
    public void readLogStatefulTest() throws IOException, InterruptedException {

        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must parse the log file entirely at first call", LOG_ENTRY_NUMBER, logParser.getLogLines().size());

        // Read a second time, no entries must be added to the logParser, it means the logReader position has not been reset and the file has not been
        // re-read.
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must only parse new entries at next calls", LOG_ENTRY_NUMBER, logParser.getLogLines().size());
    }

    @Test
    public void readLogAddedLinesTest() throws IOException, InterruptedException {

        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must parse the log file entirely at first call", LOG_ENTRY_NUMBER, logParser.getLogLines().size());

        // add a new entry at the end of the log file
        Files.write(auditTmpFile.toPath(), (VALID_HTTP_LOG_LINE + System.lineSeparator()).getBytes(StandardCharsets.UTF_8),
                StandardOpenOption.APPEND);


        // Read a second time, logReader can now read one more line and add it to the logParser.
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must parse new entries at next calls", LOG_ENTRY_NUMBER + 1, logParser.getLogLines().size());
    }

    @Test
    public void readLogRotateTest() throws IOException, InterruptedException {

        // Read a first time the log file.
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must parse the log file entirely at first call", LOG_ENTRY_NUMBER, logParser.getLogLines().size());

        // Simulate the log rotate: replace the content by a new file with one entry
        Thread.sleep(1000L);
        copyFiles(new File("src/test/resources/idp-audit-rotate.log"), auditTmpFile);

        // Read a second time, the logReader should invalidate the bufferedReader
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("", LOG_ENTRY_NUMBER, logParser.getLogLines().size());
        Assert.assertTrue(
                "When the file is modified and the logReader is not able to read any new line, then LogReader must close and nullify its " +
                        "logBufferedReader To be able to restart at next scheduling in case of log rotate",
                logReader.isLogBufferedReaderClosedAndNull());


        // Read a third time, logReader has reset the logBufferdReader and is able to parse again the new log.
        logReader.readLog(auditTmpFile.getPath());
        Assert.assertEquals("logReader must be able to restart after a log-rotate, and read new entries", LOG_ENTRY_NUMBER + 1,
                logParser.getLogLines().size());
    }


    @After
    public void tearDown() {
        auditTmpFile.delete();
        logReader.destroy();
    }


    private void copyFiles(File src, File dest) throws IOException {
        Files.write(dest.toPath(), Files.readAllBytes(src.toPath()));
    }

}
