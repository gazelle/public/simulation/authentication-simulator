package net.ihe.gazelle.simulators.authentication.adapter;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.BindingType;
import net.ihe.gazelle.simulators.authentication.model.LocationType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

@RunWith(MockitoJUnitRunner.class)
public class ShibbolethIdPAuditLogParserTest {

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethIdPAuditLogParserTest.class);

    private static final String VALID_HTTP_LOG_LINE = "2018-07-13T15:12:15.374+02:00|urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect" +
            "|_36885eb45a69d0033a39522e0a40d026|https://sp.ihe-europe.net/shibboleth|http://shibboleth.net/ns/profiles/saml2/sso/browser" +
            "|https://idp.ihe-europe.net/idp/shibboleth|urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact|_fa63bdde0a5b9dd2c76cc7454849da5a|" +
            "gthomazon|urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport|uid,gender,displayName,surname,givenName,dateOfBirth|" +
            "AAdzZWNyZXQxoTzxKXZT1wG8u+KzFp2mBI2J5Nr3a0mWfLJenJML3CUJ3RFHehWYXE6q7easCffIPqoSvkzndHzG3vP2YR4a3c60S54dlMqSLULQa/Mv6oyFfZ9TlsM0XN6ajTCiztqKLFQn/xByPcdKEGoCwQ==" +
            "|_63c2c69cfb6711916fd9e2aa94389bf1|https://idp.ihe-europe.net/idp/profile/SAML2/Redirect/SSO;jsessionid=97BC29BD813AFA5FCF44E31AE23EB1FB|urn:oasis:names:tc:SAML:2.0:status:Success|";

    private static final String VALID_SOAP_LOG_LINE = "2018-07-17T23:14:19.408+02:00|urn:oasis:names:tc:SAML:2.0:bindings:SOAP" +
            "|_20190205095704564|https://sp-clone.ihe-europe.net/shibboleth|http://shibboleth.net/ns/profiles/saml2/sso/ecp" +
            "|https://idp.ihe-europe.net/idp/shibboleth|urn:oasis:names:tc:SAML:2.0:bindings:PAOS|_28f20fe332bdb0b25aaa8ca37ee0b03c|" +
            "gthomazon|urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport|uid,gender,surname,displayName,givenName,dateOfBirth|" +
            "AAdzZWNyZXQxxdlOO0GvBVSyzPC9vdl7kSwdo5kdS9EuIdWLqHu8sqPD8K7dT83uSFjmZO9woaxcD9tUggHwcddFAW3Ep0rgnZq2Oo3dr6nK3ZdKDvOVzf0wDVdJY6LaBWvwU12oGo2Be5Up0e1yYG78o0TcpPMh|" +
            "_4027a6b83441221ebd455dc7f81711d6|https://idp.ihe-europe.net/idp/profile/SAML2/SOAP/ECP|urn:oasis:names:tc:SAML:2.0:status:Success|";

    private static final String VALID_ARTIFACT_LOG_LINE = "2019-02-05T10:34:57.382+01:00|urn:oasis:names:tc:SAML:2.0:bindings:SOAP" +
            "|_d02547506bebb6e8ede7915b9262d6ad|https://sp.ihe-europe.net/shibboleth|http://shibboleth.net/ns/profiles/saml2/query/artifact" +
            "|https://idp.ihe-europe.net/idp/shibboleth|urn:oasis:names:tc:SAML:2.0:bindings:SOAP|_a8a55e8d97794759be18e84988110c42||" +
            "urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport||" +
            "AAdzZWNyZXQxRre8IsXAjGoe5HNNHdqab91cDGGXUSUaYr/JGmjWghCKJgaZ/u3Ijmak3DijreoLy3EZenBTCTqxppr8eSDDz4IHxUhreXkQXCsK6g2R1q11uvUfzGE/LBBFaFzVBkTzQ6Cr2uhei32Bug==|" +
            "_74427829e7e68a3b0b13c7316862e6c6|https://idp.ihe-europe.net/idp/profile/SAML2/SOAP/ArtifactResolution|urn:oasis:names:tc:SAML:2.0:status:Success|";

    private static final String FAILED_SOAP_LOG_LINE = "2018-07-17T23:13:27.371+02:00|urn:oasis:names:tc:SAML:2.0:bindings:SOAP" +
            "|_de09947598784057be51bb7ec5a13dad|https://sp.ihe-europe.net/shibboleth|http://shibboleth.net/ns/profiles/saml2/sso/ecp|https://idp" +
            ".ihe-europe.net/idp/shibboleth|urn:oasis:names:tc:SAML:2.0:bindings:PAOS|_61ae162bc23b105d22aff2c4a26a720a||||||";

    private static final String INCOMPLETE_LOG_LINE = "2018-07-17T13:59:03.836+02:00||||http://shibboleth" +
            ".net/ns/profiles/saml1/query/artifact||urn:oasis:names:tc:SAML:1.0:bindings:SOAP-binding|_bfa820ac8227d9a376610eded37a7a74||||||";

    @Spy
    AttemptBuffer attemptBuffer = new AttemptBuffer();

    @InjectMocks
    LogParser logParser = new ShibbolethIdPAuditLogParser();

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX", Locale.ENGLISH);

    @Test
    public void processLineDateThresholdTest() throws ParseException {

        Mockito.doReturn(dateFormat.parse("2018-07-10T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_HTTP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        Assert.assertTrue("The parser must process a log line if its timestamp is more recent than the lastRecordDate",
                resList.size() == 1);
    }

    @Test
    public void processLineNullThresholdTest() throws ParseException {

        Mockito.doReturn(null).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_HTTP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        Assert.assertTrue("The parser must process a log line if the lastRecordDate is not defined",
                resList.size() == 1);
    }

    @Test
    public void skipProcessLineIfOldTimestampTest() throws ParseException {
        Mockito.doReturn(dateFormat.parse("2018-08-20T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_HTTP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        Assert.assertTrue("The parser must NOT process a log line if its timestamp is older than the lastRecordDate",
                resList.size() == 0);
    }

    @Test
    public void processCompleteLineTest() throws ParseException {

        Mockito.doReturn(dateFormat.parse("2018-07-10T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_HTTP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();

        AttemptRecordedByIdp res = resList.get(0);
        Assert.assertEquals(dateFormat.parse("2018-07-13T15:12:15.374+02:00"), res.getTimestamp());
        Assert.assertEquals(BindingType.HTTP_REDIRECT_BINDING, res.getUsedBinding());
        Assert.assertEquals(LocationType.REDIRECT_SSO, res.getLocation());
        Assert.assertEquals("https://sp.ihe-europe.net/shibboleth", res.getServiceProviderName());
        Assert.assertEquals("https://idp.ihe-europe.net/idp/shibboleth", res.getIdentityProviderName());
        Assert.assertEquals("gthomazon", res.getUsername());
        Assert.assertTrue(res.isAuthenticationSuccess());
        Assert.assertEquals("_63c2c69cfb6711916fd9e2aa94389bf1", res.getTokenId());

    }

    @Test
    public void detectSOAPBindingTest() throws ParseException {
        Mockito.doReturn(dateFormat.parse("2018-07-10T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_SOAP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        AttemptRecordedByIdp res = resList.get(0);

        Assert.assertEquals("SOAP Post Binding should have been detected", BindingType.SOAP_BINDING, res.getUsedBinding());
        Assert.assertEquals("Artifact location should have been detected", LocationType.SOAP_ECP, res.getLocation());
    }

    @Test
    public void detectArtifactBindingTest() throws ParseException {
        Mockito.doReturn(dateFormat.parse("2018-07-10T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(VALID_ARTIFACT_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        AttemptRecordedByIdp res = resList.get(0);

        Assert.assertEquals("SOAP Binding should have been detected", BindingType.SOAP_BINDING, res.getUsedBinding());
        Assert.assertEquals("Artifact location should have been detected", LocationType.ARTIFACT_RESOLUTION, res.getLocation());
    }

    @Test
    public void failedLogLineTest() throws ParseException {
        Mockito.doReturn(dateFormat.parse("2018-07-10T00:00:00.374+02:00")).when(attemptBuffer).getLastRecordDate();

        logParser.processLogLine(FAILED_SOAP_LOG_LINE);

        List<AttemptRecordedByIdp> resList = attemptBuffer.getAttemptsRecordedByIdp();
        AttemptRecordedByIdp res = resList.get(0);

        Assert.assertNull("No username should have been parsed", res.getUsername());
        Assert.assertNull("No assertion ID should have been parsed", res.getTokenId());
        Assert.assertFalse("Attempt should be concidered FAILED", res.isAuthenticationSuccess());
    }

}