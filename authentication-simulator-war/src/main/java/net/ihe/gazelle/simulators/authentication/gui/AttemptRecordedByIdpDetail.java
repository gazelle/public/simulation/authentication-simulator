package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.simulators.authentication.application.AuthenticationAttemptDAO;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.AuthenticationAttempt;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Scope(ScopeType.PAGE)
@Name("attemptRecordedByIdpDetail")
public class AttemptRecordedByIdpDetail implements Serializable {

    private static final long serialVersionUID = -6975899682222528988L;

    @In(value = "simulatorPreferenceProvider")
    private PreferenceProvider preferenceProvider;

    @In
    private AuthenticationAttemptDAO authenticationAttemptDAO;

    private AttemptRecordedByIdp authenticationAttempt;

    @Create
    public void init(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int id = Integer.parseInt(urlParams.get("id"));
        authenticationAttempt = authenticationAttemptDAO.getAttemptRecordedByIdpById(id);
    }

    public String getPermanentLink(AuthenticationAttempt authenticationAttempt) {
        String applicationUrl = preferenceProvider.getString("application_url");
        if (applicationUrl != null) {
            return applicationUrl + "/idp/view.seam" + "?id=" + authenticationAttempt.getId();
        } else {
            return "#";
        }
    }

    public AttemptRecordedByIdp getAuthenticationAttempt() {
        return authenticationAttempt;
    }

    public void setAuthenticationAttempt(AttemptRecordedByIdp authenticationAttempt) {
        this.authenticationAttempt = authenticationAttempt;
    }

    public String getCompleteLocationURL(){
        if(preferenceProvider.getString("idp_fqdn") != null && authenticationAttempt.getLocation() != null) {
            return (preferenceProvider.getString("idp_fqdn") + authenticationAttempt.getLocation().getLabel());
        } else {
            return null;
        }
    }
}
