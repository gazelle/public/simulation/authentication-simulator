package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.simulators.authentication.model.AuthenticationAttempt;

public interface SimuView<T extends AuthenticationAttempt> {

    FilterDataModel<T> getAuthenticationAttempts();

    Filter<T> getFilter();

    void refresh();

    void resetFilter();

    String navigateToAttemptView(T authenticationAttempt);

}
