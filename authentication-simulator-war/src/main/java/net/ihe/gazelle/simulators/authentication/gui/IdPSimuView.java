package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicDate;
import net.ihe.gazelle.simulators.authentication.application.AuthenticationAttemptCollector;
import net.ihe.gazelle.simulators.authentication.application.AuthenticationAttemptDAO;
import net.ihe.gazelle.simulators.authentication.application.IdPConnectionException;
import net.ihe.gazelle.simulators.authentication.application.IdpException;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdpQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

@Scope(ScopeType.PAGE)
@Name("idpSimuView")
public class IdPSimuView extends AbstractSimuView<AttemptRecordedByIdp> implements Serializable {

    private static final long serialVersionUID = -2589167770222844330L;
    
    private static final Logger LOG = LoggerFactory.getLogger(IdPSimuView.class);
    private static final String PLEASE_CONTACT_AN_ADMIN = "Please contact an administrator";

    @In(value = "authenticationAttemptCollector")
    private AuthenticationAttemptCollector authenticationAttemptCollector;

    @In
    private AuthenticationAttemptDAO authenticationAttemptDAO;

    @Create
    public void init() {
        fetchIdPLogs();
    }

    @Override
    public void refresh() {
        fetchIdPLogs();
        getAuthenticationAttempts().resetCache();
    }

    public String navigateToAttemptView(AttemptRecordedByIdp attemptRecordedByIdp) {
        if (attemptRecordedByIdp != null) {
            return "/idp/view.xhtml" + "?id=" + attemptRecordedByIdp.getId();
        } else {
            return null;
        }
    }

    /*
     * Map filter elements to HQL Query
     */
    HQLCriterionsForFilter<AttemptRecordedByIdp> getHQLCriterionsForFilter() {
        final AttemptRecordedByIdpQuery query = authenticationAttemptDAO.getAttemptRecordedByIdpQuery();
        final HQLCriterionsForFilter<AttemptRecordedByIdp> criterionsForFilter = query.getHQLCriterionsForFilter();

        final HQLSafePathBasicDate<Date> timestamp = query.timestamp();
        timestamp.setCriterionWithoutTime(TimeZone.getDefault());

        criterionsForFilter.addPath("timestamp", timestamp);
        criterionsForFilter.addPath("username", query.username());
        criterionsForFilter.addPath("authenticationSuccess", query.authenticationSuccess());
        criterionsForFilter.addPath("usedBinding", query.usedBinding());
        criterionsForFilter.addPath("serviceProviderName", query.serviceProviderName());
        return criterionsForFilter;
    }

    private void fetchIdPLogs() {
        try {
            authenticationAttemptCollector.retrieveIdPLogs();
        } catch (IdPConnectionException e) {
            String message = String.format("Error contacting IdP Simulator Adapter: %s", e.getMessage());
            LOG.error(message);
            FacesMessages.instance().add(Severity.ERROR, message + System.lineSeparator() + PLEASE_CONTACT_AN_ADMIN);
        } catch (IdpException e) {
            if (e.getPendingExceptions() != null && !e.getPendingExceptions().isEmpty()) {
                for (Exception idpE : e.getPendingExceptions()) {
                    String message = String.format("Error reported by IdP Adapter: %s", idpE.getMessage());
                    LOG.error(message);
                    FacesMessages.instance().add(Severity.ERROR, message);
                }
                FacesMessages.instance().add(Severity.ERROR, PLEASE_CONTACT_AN_ADMIN);
            } else {
                LOG.error(e.getMessage());
                FacesMessages.instance().add(Severity.ERROR, e.getMessage() + System.lineSeparator() + PLEASE_CONTACT_AN_ADMIN);
            }
        }
    }

}
