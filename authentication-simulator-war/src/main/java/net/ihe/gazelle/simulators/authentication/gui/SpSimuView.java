package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.paths.HQLSafePathBasicDate;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedBySp;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedBySpQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

@Scope(ScopeType.PAGE)
@Name("spSimuView")
public class SpSimuView extends AbstractSimuView<AttemptRecordedBySp> implements Serializable {

    private static final long serialVersionUID = -2405637538326439443L;

    @Override
    public String navigateToAttemptView(AttemptRecordedBySp authenticationAttempt) {
        if (authenticationAttempt != null) {
            return "/xsu/view.xhtml" + "?id=" + authenticationAttempt.getId();
        } else {
            return null;
        }
    }

    @Override
    HQLCriterionsForFilter<AttemptRecordedBySp> getHQLCriterionsForFilter() {
        final AttemptRecordedBySpQuery authenticationAttemptQueryQuery = new AttemptRecordedBySpQuery();
        final HQLCriterionsForFilter<AttemptRecordedBySp> result = authenticationAttemptQueryQuery.getHQLCriterionsForFilter();
        final HQLSafePathBasicDate<Date> timestamp = authenticationAttemptQueryQuery.timestamp();
        timestamp.setCriterionWithoutTime(TimeZone.getDefault());
        result.addPath("timestamp", timestamp);
        result.addPath("username", authenticationAttemptQueryQuery.username());
        result.addPath("authentication_success", authenticationAttemptQueryQuery.authenticationSuccess());
        result.addPath("used_binding", authenticationAttemptQueryQuery.usedBinding());
        return result;
    }
}
