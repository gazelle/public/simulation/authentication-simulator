package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.simulators.authentication.application.AuthenticationAttemptDAO;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedBySp;
import net.ihe.gazelle.simulators.authentication.model.AuthenticationAttempt;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Scope(ScopeType.PAGE)
@Name("attemptRecordedBySpDetail")
public class AttemptRecordedBySpDetail implements Serializable {

    private static final long serialVersionUID = -3850596224243023677L;

    @In(value = "simulatorPreferenceProvider")
    private PreferenceProvider preferenceProvider;

    @In
    private AuthenticationAttemptDAO authenticationAttemptDAO;

    private AttemptRecordedBySp authenticationAttempt;

    @Create
    public void init(){
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        int id = Integer.parseInt(urlParams.get("id"));
        authenticationAttempt = authenticationAttemptDAO.getAttemptRecordedBySpById(id);
    }

    public String getPermanentLink(AuthenticationAttempt authenticationAttempt) {
        String applicationUrl = preferenceProvider.getString("application_url");
        if (applicationUrl != null) {
            return applicationUrl + "/xsu/view.seam" + "?id=" + authenticationAttempt.getId();
        } else {
            return "#";
        }
    }

    public AttemptRecordedBySp getAuthenticationAttempt() {
        return authenticationAttempt;
    }

    public void setAuthenticationAttempt(AttemptRecordedBySp authenticationAttempt) {
        this.authenticationAttempt = authenticationAttempt;
    }
}
