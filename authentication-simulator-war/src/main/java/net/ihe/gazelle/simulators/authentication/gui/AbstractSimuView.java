package net.ihe.gazelle.simulators.authentication.gui;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.authentication.model.AuthenticationAttempt;

import javax.faces.context.FacesContext;
import java.io.Serializable;

public abstract class AbstractSimuView<T extends AuthenticationAttempt> implements SimuView<T>, Serializable {

    private static final long serialVersionUID = 6545970416941737272L;
    private Filter<T> filter;
    private FilterDataModel<T> authenticationAttempts;

    @Override
    public Filter<T> getFilter() {
        if (filter == null) {
            filter = new Filter<T>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    @Override
    public FilterDataModel<T> getAuthenticationAttempts() {
        if (this.authenticationAttempts == null) {
            this.authenticationAttempts = new FilterDataModel<T>(getFilter()) {
                @Override
                protected Object getId(T authenticationAttempt) {
                    return authenticationAttempt.getId();
                }
            };
        }
        return this.authenticationAttempts;
    }

    @Override
    public void refresh() {
        getAuthenticationAttempts().resetCache();
    }

    @Override
    public void resetFilter() {
        getFilter().clear();
        refresh();
    }

    abstract HQLCriterionsForFilter<T> getHQLCriterionsForFilter();

}
