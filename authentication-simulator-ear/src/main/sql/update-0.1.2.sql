INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'idp_fqdn', 'https://idp.ihe-europe.net/');
ALTER TABLE auth_attempt_recorded_by_idp ADD COLUMN location character varying(255);
update auth_attempt_recorded_by_idp SET used_binding = 'SOAP_BINDING' where used_binding = 'SOAP_POST_BINDING';
update auth_attempt_recorded_by_idp SET used_binding = 'SOAP_BINDING' where used_binding = 'ARTIFACT_OVER_SOAP_BINDING';

INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://ehealthsuisse.ihe-europe.net/metadata/idp-metadata.xml ', 'idp_metadata');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://ehealthsuisse.ihe-europe.net/idp/profile/SAML2/POST/SSO', 'authentification_request');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://ehealthsuisse.ihe-europe.net/idp/profile/SAML2/SOAP/ArtifactResolution', 'artifact_resolution');
INSERT INTO public.app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://ehealthsuisse.ihe-europe.net/idp/shibboleth', 'idp_entity_id');