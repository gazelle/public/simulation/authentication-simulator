package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.preferences.PreferenceProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RunWith(MockitoJUnitRunner.class)
public class IdPSimulatorWSFactoryTest {

    private static final Logger LOG = LoggerFactory.getLogger(IdPSimulatorWSFactoryTest.class);

    private static final int TESTING_PORT = 22222;
    private static WsdlServerStub wsdlServerStub = new WsdlServerStub(TESTING_PORT);

    @Mock
    private PreferenceProvider preferenceProvider;

    @InjectMocks
    private IdPSimulatorFactory idPSimulatorFactory = new IdPSimulatorWSFactory();


    @Test
    public void idPAdatperClientTest() throws IdPConnectionException {

        setUpWsdlServerStup();
        Mockito.when(preferenceProvider.getString("idp_adapter_wsdl_endpoint"))
                .thenReturn("http://localhost:" + TESTING_PORT + "/idp-adapter-ejb/IdPSimulatorService/IdPSimulatorWS?wsdl");


        IdPSimulator idPSimulator = idPSimulatorFactory.getIdPSimulator();
        Assert.assertNotNull("IdPSimulatorWSFactory must have established an http connection with the idp adapter webservice", idPSimulator);


    }

    @Test(expected = IdPConnectionException.class)
    public void noIdPAdatperURLTest() throws IdPConnectionException {
        Mockito.when(preferenceProvider.getString("idp_adapter_wsdl_endpoint")).thenReturn(null);
        idPSimulatorFactory.getIdPSimulator();
        Assert.fail("idpSimulatorWSFactory must detect idp_adapter_wsdl_endpoint not defined");
    }

    @Test(expected = IdPConnectionException.class)
    public void wrongIdPAdatperURLTest() throws IdPConnectionException {
        Mockito.when(preferenceProvider.getString("idp_adapter_wsdl_endpoint")).thenReturn("thisisnotanurl");
        idPSimulatorFactory.getIdPSimulator();
        Assert.fail("idpSimulatorWSFactory must detect idp_adapter_wsdl_endpoint wrongly defined");
    }

    @Test(expected = IdPConnectionException.class)
    public void notReachableIdPAdatperTest() throws IdPConnectionException {
        Mockito.when(preferenceProvider.getString("idp_adapter_wsdl_endpoint"))
                .thenReturn("http://localhost:22404/idp-adapter-ejb/IdPSimulatorService/IdPSimulatorWS?wsdl");
        idPSimulatorFactory.getIdPSimulator();
        Assert.fail("idpSimulatorWSFactory must detect if idp adapter ws is not reachable");
    }


    private synchronized void setUpWsdlServerStup() {
        wsdlServerStub.start();
    }

    @After
    public synchronized void teadDownWsdlServerStup() {
        try {
            wsdlServerStub.stop();
        } catch (IllegalStateException e) {
            //ignore, already stopped
        }
    }

}
