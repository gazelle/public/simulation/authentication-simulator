package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.BindingType;
import net.ihe.gazelle.simulators.authentication.model.LocationType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.persistence.EntityManager;
import javax.xml.ws.WebServiceException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class AuthenticationAttemptCollectorTest {

    @Mock
    private EntityManager entityManager;
    @Mock
    private AuthenticationAttemptDAO authenticationAttemptDAO;
    @Mock
    private IdPSimulatorFactory idPSimulatorMockFactory;

    @InjectMocks
    private AuthenticationAttemptCollector authenticationAttemptCollector = new AuthenticationAttemptCollector();

    private IdPSimulator idPSimulator;

    private List<AttemptRecordedByIdp> attemptRecordedByIdps = new ArrayList<>();

    @Before
    public void setUp() throws IdPConnectionException {
        idPSimulator = Mockito.mock(IdPSimulator.class);

        Mockito.when(idPSimulatorMockFactory.getIdPSimulator()).thenReturn(idPSimulator);
        Mockito.when(entityManager.merge(ArgumentMatchers.any(AttemptRecordedByIdp.class))).then(new Answer<AttemptRecordedByIdp>() {
            @Override
            public AttemptRecordedByIdp answer(InvocationOnMock invocationOnMock) throws Throwable {
                return (AttemptRecordedByIdp) invocationOnMock.getArguments()[0];
            }
        });
    }

    @Test
    public void retrieveNewIdPLogsTest() throws IdpException, IdPConnectionException {
        initAttempts(attemptRecordedByIdps);
        Mockito.when(idPSimulator.fetchAuthenticationAttempts()).thenReturn(attemptRecordedByIdps);
        Mockito.when(authenticationAttemptDAO.isAttemptRecordedByIdpAlreadyPersisted(ArgumentMatchers.any(AttemptRecordedByIdp.class)))
                .thenReturn(false);

        authenticationAttemptCollector.retrieveIdPLogs();

        Mockito.verify(entityManager, Mockito.times(3).description("authenticationAttemptCollector must save every new log returned"))
                .merge(ArgumentMatchers.any(AttemptRecordedByIdp.class));

    }


    @Test
    public void retrieveNoIdPLogsTest() throws IdpException, IdPConnectionException {

        Mockito.when(idPSimulator.fetchAuthenticationAttempts()).thenReturn(attemptRecordedByIdps);

        authenticationAttemptCollector.retrieveIdPLogs();

        Mockito.verify(entityManager, Mockito.never().description("authenticationAttemptCollector must save nothing if no logs returned"))
                .merge(ArgumentMatchers.any(AttemptRecordedByIdp.class));

    }

    @Test
    public void retrieveOldIdPLogsTest() throws IdpException, IdPConnectionException {
        initAttempts(attemptRecordedByIdps);
        Mockito.when(idPSimulator.fetchAuthenticationAttempts()).thenReturn(attemptRecordedByIdps);

        Mockito.when(authenticationAttemptDAO.isAttemptRecordedByIdpAlreadyPersisted(attemptRecordedByIdps.get(0)))
                .thenReturn(true);
        Mockito.when(authenticationAttemptDAO.isAttemptRecordedByIdpAlreadyPersisted(attemptRecordedByIdps.get(1)))
                .thenReturn(false);
        Mockito.when(authenticationAttemptDAO.isAttemptRecordedByIdpAlreadyPersisted(attemptRecordedByIdps.get(2)))
                .thenReturn(false);

        authenticationAttemptCollector.retrieveIdPLogs();

        Mockito.verify(entityManager, Mockito.times(2).description("authenticationAttemptCollector must not save attempts already recorded"))
                .merge(ArgumentMatchers.any(AttemptRecordedByIdp.class));

    }

    @Test(expected = IdPConnectionException.class)
    public void retrieveIdPLogsConnectionClosedTest() throws IdpException, IdPConnectionException {
        Mockito.when(idPSimulator.fetchAuthenticationAttempts()).thenThrow(new WebServiceException());
        authenticationAttemptCollector.retrieveIdPLogs();
        Assert.fail("authenticationAttemptCollector must detect broken connection and raise and error");
    }

    private void initAttempts(List<AttemptRecordedByIdp> attemptRecordedByIdps) {
        attemptRecordedByIdps.clear();
        attemptRecordedByIdps.add(new AttemptRecordedByIdp(new Date(), "riri", "SP-1", "IDP mock", true, BindingType.HTTP_REDIRECT_BINDING,
                "0123456789", LocationType.REDIRECT_SSO));
        attemptRecordedByIdps.add(new AttemptRecordedByIdp(new Date(), "fifi", "SuperSP", "IDP mock", true, BindingType.HTTP_REDIRECT_BINDING,
                "491203291", LocationType.REDIRECT_SSO));
        attemptRecordedByIdps.add(new AttemptRecordedByIdp(new Date(), "loulou", "SP-1", "IDP mock", true, BindingType.SOAP_BINDING,
                "0920394812094", LocationType.SOAP_ECP));
    }


}
