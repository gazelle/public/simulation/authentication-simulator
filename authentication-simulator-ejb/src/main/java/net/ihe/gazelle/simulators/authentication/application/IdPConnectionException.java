package net.ihe.gazelle.simulators.authentication.application;

public class IdPConnectionException extends Exception {


    public IdPConnectionException(String s) {
        super(s);
    }

    public IdPConnectionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public IdPConnectionException(Throwable throwable) {
        super(throwable);
    }

    protected IdPConnectionException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
