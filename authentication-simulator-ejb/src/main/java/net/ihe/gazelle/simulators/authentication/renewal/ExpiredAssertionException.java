package net.ihe.gazelle.simulators.authentication.renewal;

public class ExpiredAssertionException extends RuntimeException {
    public ExpiredAssertionException() {
    }

    public ExpiredAssertionException(String s) {
        super(s);
    }

    public ExpiredAssertionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ExpiredAssertionException(Throwable throwable) {
        super(throwable);
    }
}
