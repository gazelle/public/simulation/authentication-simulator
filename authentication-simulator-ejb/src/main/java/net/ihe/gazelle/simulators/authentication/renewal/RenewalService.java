package net.ihe.gazelle.simulators.authentication.renewal;


import org.opensaml.saml2.core.Assertion;

public interface RenewalService {

   String SAML_2_0_ASSERTION_NS = "urn:oasis:names:tc:SAML:2.0:assertion";
   String ASSERTION_ELEMENT_NAME = "Assertion";

   String SAML_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:assertion";

   String ISSUER_TAGNAME = "Issuer";

   String NAME_ID_TAGNAME = "NameID";


   Assertion renew(Assertion assertion);

}
