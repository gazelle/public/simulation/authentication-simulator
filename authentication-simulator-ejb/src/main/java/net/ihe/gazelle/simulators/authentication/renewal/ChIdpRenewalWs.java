package net.ihe.gazelle.simulators.authentication.renewal;

import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.namespace.QName;
import javax.xml.ws.Action;
import javax.xml.ws.BindingType;


@Local
@WebService(
      serviceName = "ch-idp",
      name = "renewal",
      targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/wsdl"
)
@XmlSeeAlso({
      org.apache.cxf.ws.security.sts.provider.model.ObjectFactory.class,
      org.apache.cxf.ws.security.sts.provider.model.wstrust14.ObjectFactory.class,
      org.apache.cxf.ws.security.sts.provider.model.secext.ObjectFactory.class,
      org.apache.cxf.ws.security.sts.provider.model.utility.ObjectFactory.class,
      org.apache.cxf.ws.security.sts.provider.model.xmldsig.ObjectFactory.class,
      org.apache.cxf.ws.addressing.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
public interface ChIdpRenewalWs {

   static final String SOAP_12_ENV_NS = "http://www.w3.org/2003/05/soap-envelope";
   static final QName SENDER_ENV_FAULT_CODE = new QName(SOAP_12_ENV_NS, "Sender");
   static final QName RECEIVER_ENV_FAULT_CODE = new QName(SOAP_12_ENV_NS, "Receiver");

   static final String WSTRUST_13_NS = "http://docs.oasis-open.org/ws-sx/ws-trust/200512";
   static final String RENEW_REQUEST_TYPE = WSTRUST_13_NS + "/Renew";
   static final QName REQUEST_TYPE_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "RequestType");
   static final QName TOKEN_TYPE_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "TokenType");
   static final QName INVALID_REQUEST_WSTRUST_FAULT_SUB_CODE = new QName(WSTRUST_13_NS, "InvalidRequest");

   static final String SAML2_TOKEN_PROFILE_TYPE =
         "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0";

   @WebResult(name = "RequestSecurityTokenResponse", targetNamespace = WSTRUST_13_NS, partName = "response")
   @Action(input = WSTRUST_13_NS + "/RST/Renew", output = WSTRUST_13_NS + "/RST/RenewFinal")
   @WebMethod(operationName = "Renew")
   RequestSecurityTokenResponseType renew(
         @WebParam(partName = "request", name = "RequestSecurityToken", targetNamespace = WSTRUST_13_NS)
               RequestSecurityTokenType request
   );
}
