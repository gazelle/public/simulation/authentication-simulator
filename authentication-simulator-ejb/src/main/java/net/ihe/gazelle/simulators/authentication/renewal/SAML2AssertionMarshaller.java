package net.ihe.gazelle.simulators.authentication.renewal;

import org.opensaml.DefaultBootstrap;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.impl.AssertionMarshaller;
import org.opensaml.saml2.core.impl.AssertionUnmarshaller;
import org.opensaml.xml.ConfigurationException;
import org.opensaml.xml.io.MarshallingException;
import org.opensaml.xml.io.UnmarshallingException;
import org.w3c.dom.Element;

public class SAML2AssertionMarshaller {

   static {
      try {
         // Initialize opensaml
         DefaultBootstrap.bootstrap();
      } catch (ConfigurationException e) {
         throw new RuntimeException(e);
      }
   }

   public Element marshall(Assertion assertion) throws MarshallingException {
      return new AssertionMarshaller().marshall(assertion);
   }

   public Assertion unmarshall(Element assertionElement) throws UnmarshallingException {
      return (Assertion) new AssertionUnmarshaller().unmarshall(assertionElement);
   }

}
