package net.ihe.gazelle.simulators.authentication.renewal;

import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.opensaml.saml2.core.Assertion;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Name("renewalService")
@AutoCreate
public class RenewalServiceImpl implements RenewalService {

    public final String SUBJECT_CONFIRMATION_DATA_TAGNAME = "SubjectConfirmationData";
    public final String INVALID_REQUEST_ERROR_MESSAGE = "wst:InvalidRequest";

    public final String EXPIRED_DATA_ERROR_MESSAGE = "wst:ExpiredData";
    private final String NOT_ON_OR_AFTER_ATTRIBUTE_NAME = "NotOnOrAfter";
    private SAML2AssertionMarshaller assertionMarshaller = new SAML2AssertionMarshaller();

    @Override
    public Assertion renew(Assertion assertion) {

        // TODO to implement
        // 1. verify Assertion is valid, throw IllegalArgumentException is not.

        //TODO
        if (assertion == null)
            throw new IllegalArgumentException("Assertion must be defined or not null");

        checkValidityTime(assertion);

        // 3. extract Issuer, username (previously validated in (1)).
        String issuer = assertion.getConditions().getAudienceRestrictions().get(0).getAudiences().get(0).getAudienceURI();
        String username = assertion.getSubject().getNameID().getValue();

        // 6. Call the SAMLAuthClient and return the new Assertion.
        SAMLAuthnClient shibbolethClient = new ShibbolethAuthnClient();
        return shibbolethClient.renew(issuer, username, "azerty");

    }

    private void checkValidityTime(Assertion assertion) {

        Date expirationDate = assertion.getConditions().getNotOnOrAfter().toDate();
        Date nowDate = new Date();

        long diffInMillies = Math.abs(nowDate.getTime() - expirationDate.getTime());
        long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);

        if (diff > 120)
            throw new ExpiredAssertionException("renewal is not allowed two hours beyond the assertion expiration");

    }
}
