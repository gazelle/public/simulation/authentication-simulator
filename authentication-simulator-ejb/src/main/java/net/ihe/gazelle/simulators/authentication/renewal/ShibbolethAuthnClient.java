package net.ihe.gazelle.simulators.authentication.renewal;

//import org.apache.commons.httpclient.HttpClient;
import net.ihe.gazelle.simulators.authentication.application.IdpException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpAuthenticator;
import org.apache.xml.security.utils.Base64;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.Instant;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class ShibbolethAuthnClient implements SAMLAuthnClient{

    private static final Logger LOG = LoggerFactory.getLogger(ShibbolethAuthnClient.class);
    private static final String POST_ENDPOINT = "https://ehealthsuisse.ihe-europe.net/idp/profile/SAML2/SOAP/ECP";
    private static final String UNABLE_TO_RENEW_ERROR_MESSAGE = "wst:UnableToRenew";
    private static final String INVALID_REQUEST_ERROR_MESSAGE = "wst:InvalidRequest";

    private static final String SAMLP_NAMESPACE = "urn:oasis:names:tc:SAML:2.0:protocol";

    private static final String AUTHNREQUEST_TAGNAME = "AuthnRequest";




    private SAML2AssertionMarshaller assertionMarshaller;

    ShibbolethAuthnClient(){
        assertionMarshaller = new SAML2AssertionMarshaller();
    }

    @Override
    public Assertion renew(String issuer, String username, String password) {
        try {
            String requestBody = completeRequestTemplate(issuer);
            HttpClient client = new DefaultHttpClient();
            HttpPost post = buildPostRequest(requestBody, username, password);
            HttpResponse response = client.execute(post);

            return parseResponse(response);

        } catch (IOException e) {
            throw new AuthnClientException(e);
        }
    }

    private static HttpPost buildPostRequest(String requestBody, String username, String password) throws UnsupportedEncodingException {
        HttpPost post = new HttpPost(POST_ENDPOINT);
        String encoding = Base64.encode((username + ":" + password).getBytes());
        post.addHeader("Authorization", "Basic " + encoding);
        post.addHeader("content-type", "text/xml");
        post.setEntity(new StringEntity(requestBody));
        return post;
    }

    private Assertion parseResponse(HttpResponse response) {
        try {
            if (response.getStatusLine().getStatusCode() != 200)
                throw new AuthnClientException(UNABLE_TO_RENEW_ERROR_MESSAGE);

            HttpEntity body = response.getEntity();
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(body.getContent());
            doc.getDocumentElement().normalize();

            Element assertionElement = (Element)doc.getElementsByTagNameNS(RenewalService.SAML_2_0_ASSERTION_NS, RenewalService.ASSERTION_ELEMENT_NAME).item(0);

            LOG.info(printRequest(assertionElement));

            return assertionMarshaller.unmarshall(assertionElement);

        } catch (ParserConfigurationException | IOException | SAXException | UnmarshallingException |
                 TransformerException e) {
            throw new AuthnClientException(UNABLE_TO_RENEW_ERROR_MESSAGE);
        }
    }

    private String completeRequestTemplate(String issuer) {

        try {
            Document doc = getDocumentForRequest();
            injectRequestDataInDoc(issuer, doc);
            return printRequest(doc);
        } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
            throw new AuthnClientException(UNABLE_TO_RENEW_ERROR_MESSAGE);
        }
    }

    private static String printRequest(Node node) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(node), new StreamResult(writer));
        return writer.getBuffer().toString();
    }

    private static void injectRequestDataInDoc(String issuer, Document doc) {
        Element authnRequest = (Element) doc.getElementsByTagNameNS(SAMLP_NAMESPACE, AUTHNREQUEST_TAGNAME).item(0);

        //In my implementation there was a 2 hour delay because Date was taking the Date of my computer (UTC+2) and not from UTC
        //Duration d = Duration.standardHours(2);
        //Date issueInstant = Instant.now().minus(d).toDate();
        DateTime issueInstantDate = new DateTime(DateTimeZone.UTC);
        String issueInstant = issueInstantDate.toString("yyyyMMddHHmmssSSS");



        //Date issueInstant = new Date();

        //SimpleDateFormat formatterId= new SimpleDateFormat("yyyyMMddHHmmssSSS");
        //String assertionId = formatterId.format(issueInstant);
        authnRequest.setAttribute("ID", issueInstant);

        //SimpleDateFormat formatterIssueInstant= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        //SimpleDateFormat formatterIssueInstant= new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        //authnRequest.setAttribute("IssueInstant", formatterIssueInstant.format(issueInstant));
        authnRequest.setAttribute("IssueInstant", issueInstantDate.toString("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));

        authnRequest.getElementsByTagNameNS(RenewalService.SAML_NAMESPACE, RenewalService.ISSUER_TAGNAME).item(0).setTextContent(issuer);
    }

    private Document getDocumentForRequest() throws ParserConfigurationException, SAXException, IOException {
        InputStream requestTemplateIS = getClass().getResourceAsStream("/request.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document doc = builder.parse(requestTemplateIS);
        doc.getDocumentElement().normalize();
        return doc;
    }
}
