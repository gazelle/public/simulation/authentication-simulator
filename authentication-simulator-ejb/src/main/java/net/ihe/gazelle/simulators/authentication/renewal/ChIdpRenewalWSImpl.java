package net.ihe.gazelle.simulators.authentication.renewal;

import org.apache.cxf.ws.security.sts.provider.model.LifetimeType;
import org.apache.cxf.ws.security.sts.provider.model.ObjectFactory;
import org.apache.cxf.ws.security.sts.provider.model.RenewTargetType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenResponseType;
import org.apache.cxf.ws.security.sts.provider.model.RequestSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.RequestedSecurityTokenType;
import org.apache.cxf.ws.security.sts.provider.model.utility.AttributedDateTime;
import org.apache.ws.security.util.XmlSchemaDateFormat;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.ws.api.annotation.EndpointConfig;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.xml.io.MarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.ejb.Stateless;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPFault;
import javax.xml.ws.BindingType;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.soap.SOAPFaultException;
import java.text.MessageFormat;
import java.util.Date;

@Stateless
@Name("chIdpRenewalWS")
@WebService(
      serviceName = "ch-idp",
      name = "renewal",
      targetNamespace = "http://docs.oasis-open.org/ws-sx/ws-trust/200512/wsdl"
)
@EndpointConfig(configName = "Seam WebService Endpoint")
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)
@HandlerChain(file = "/META-INF/soap-handlers.xml")
public class ChIdpRenewalWSImpl implements ChIdpRenewalWs {

   private static final Logger LOG = LoggerFactory.getLogger(ChIdpRenewalWSImpl.class);
   public static final QName RENEW_TARGET_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "RenewTarget");
   private static final ObjectFactory WS_TRUST_FACTORY = new ObjectFactory();
   private static final org.apache.cxf.ws.security.sts.provider.model.utility.ObjectFactory UTIL_FACTORY =
         new org.apache.cxf.ws.security.sts.provider.model.utility.ObjectFactory();

   @In
   private RenewalService renewalService;

   private SAML2AssertionMarshaller assertionMarshaller = new SAML2AssertionMarshaller();

   public ChIdpRenewalWSImpl() {
   }

   public ChIdpRenewalWSImpl(RenewalService renewalService, SAML2AssertionMarshaller assertionMarshaller) {
      this.renewalService = renewalService;
      this.assertionMarshaller = assertionMarshaller;
   }

   @Override
   public RequestSecurityTokenResponseType renew(RequestSecurityTokenType request) {
      assertValidRequest(request);
      try {
         return wrapInResponse(
               renewalService.renew(assertionMarshaller.unmarshall(getAssertionElement(request)))
         );
      } catch (Exception e) {
         LOG.error("Unexpected error in renewal WS.", e);
         throw buildReceiverSoapFault(e);
      }
   }

   private void assertValidRequest(RequestSecurityTokenType request) {
      assertRenewRequest(request);
      assertSAMLTokenType(request);
      assertRenewTarget(request);
   }

   private void assertRenewTarget(RequestSecurityTokenType request) {
      if (!hasAssertionRenewTarget(request)) {
         throw buildSenderSoapFault("RenewTarget must contain an SAML Assertion.",
               INVALID_REQUEST_WSTRUST_FAULT_SUB_CODE);
      }
   }

   private boolean hasAssertionRenewTarget(RequestSecurityTokenType request) {
      return getAssertionElement(request) != null;
   }

   private void assertSAMLTokenType(RequestSecurityTokenType request) {
      if (!isSAMLTokenType(request)) {
         throw buildSenderSoapFault("TokenType must be " + SAML2_TOKEN_PROFILE_TYPE,
               INVALID_REQUEST_WSTRUST_FAULT_SUB_CODE);
      }
   }

   private boolean isSAMLTokenType(RequestSecurityTokenType request) {
      return hasElementWithValue(request, TOKEN_TYPE_WSTRUST_ELEMENT, String.class, SAML2_TOKEN_PROFILE_TYPE);
   }

   private void assertRenewRequest(RequestSecurityTokenType request) {
      if (!isRenewRequest(request)) {
         throw buildSenderSoapFault("RequestType must be " + RENEW_REQUEST_TYPE,
               INVALID_REQUEST_WSTRUST_FAULT_SUB_CODE);
      }
   }

   private boolean isRenewRequest(RequestSecurityTokenType request) {
      return hasElementWithValue(request, REQUEST_TYPE_WSTRUST_ELEMENT, String.class, RENEW_REQUEST_TYPE);
   }

   private Element getAssertionElement(RequestSecurityTokenType request) {
      RenewTargetType renewTarget = getElementValue(request, RENEW_TARGET_WSTRUST_ELEMENT, RenewTargetType.class);
      return renewTarget != null ? getAssertionElement(renewTarget) : null;
   }

   private Element getAssertionElement(RenewTargetType renewTarget) {
      if (renewTarget.getAny() != null) {
         if (Element.class.isAssignableFrom(renewTarget.getAny().getClass())) {
            Element renewTargetValueElement = (Element) renewTarget.getAny();
            if (RenewalService.SAML_2_0_ASSERTION_NS.equals(renewTargetValueElement.getNamespaceURI())
                  && RenewalService.ASSERTION_ELEMENT_NAME.equals(renewTargetValueElement.getLocalName())) {
               return renewTargetValueElement;
            }
         } else {
            throw new ClassCastException(
                  MessageFormat.format("Value of element {0} cannot be cast as {1}.", RENEW_TARGET_WSTRUST_ELEMENT,
                        Element.class));
         }
      }
      return null;
   }

   private <T> boolean hasElementWithValue(RequestSecurityTokenType request,
                                           QName elementName,
                                           Class<T> valueClass,
                                           T expectedValue) {
      return expectedValue.equals(getElementValue(request, elementName, valueClass));
   }

   private <T> T getElementValue(RequestSecurityTokenType request,
                                 QName elementName,
                                 Class<T> valueClass) {
      for (Object any : request.getAny()) {
         if (elementName.equals(((JAXBElement<?>) any).getName())) {
            if (((JAXBElement<?>) any).getDeclaredType().isAssignableFrom(valueClass)) {
               return ((JAXBElement<T>) any).getValue();
            } else {
               throw new ClassCastException(
                     MessageFormat.format("Value of element {0} cannot be cast as {1}.", elementName, valueClass));
            }
         }
      }
      return null;
   }


   private RequestSecurityTokenResponseType wrapInResponse(Assertion assertion) throws MarshallingException {
      RequestSecurityTokenResponseType response =
            WS_TRUST_FACTORY.createRequestSecurityTokenResponseType();
      response.getAny().add(buildSAML2TokenType());
      response.getAny().add(buildRequestedToken(assertionMarshaller.marshall(assertion)));
      response.getAny().add(buildLifetime(assertion));
      return response;
   }

   private JAXBElement<String> buildSAML2TokenType() {
      return WS_TRUST_FACTORY.createTokenType(SAML2_TOKEN_PROFILE_TYPE);
   }

   private JAXBElement<RequestedSecurityTokenType> buildRequestedToken(Element assertionElement) {
      RequestedSecurityTokenType requestedTokenType =
            WS_TRUST_FACTORY.createRequestedSecurityTokenType();
      JAXBElement<RequestedSecurityTokenType> requestedToken =
            WS_TRUST_FACTORY.createRequestedSecurityToken(
                  requestedTokenType);
      requestedTokenType.setAny(assertionElement);
      return requestedToken;
   }

   private JAXBElement<LifetimeType> buildLifetime(Assertion assertion) {
      LifetimeType lifetimeType = WS_TRUST_FACTORY.createLifetimeType();
      lifetimeType.setCreated(
            buildAttributedDateTime(assertion.getConditions().getNotBefore().toDate())
      );
      lifetimeType.setExpires(
            buildAttributedDateTime(assertion.getConditions().getNotOnOrAfter().toDate())
      );
      return WS_TRUST_FACTORY.createLifetime(lifetimeType);
   }

   private static AttributedDateTime buildAttributedDateTime(Date date) {
      XmlSchemaDateFormat fmt = new XmlSchemaDateFormat();
      AttributedDateTime created = UTIL_FACTORY.createAttributedDateTime();
      created.setValue(fmt.format(date));
      return created;
   }


   private SOAPFaultException buildSenderSoapFault(String reasonText, QName faultSubCode) {
      return buildSoapFault(reasonText, SENDER_ENV_FAULT_CODE, faultSubCode);
   }

   private SOAPFaultException buildReceiverSoapFault(Throwable e) {
      return buildSoapFault(
            e.getMessage() != null ? e.getMessage() : "Unexpected error",
            RECEIVER_ENV_FAULT_CODE,
            new QName(e.getClass().getName(), e.getClass().getSimpleName())
      );
   }

   private SOAPFaultException buildSoapFault(String reasonText, QName faultCode, QName faultSubCode) {
      try {
         SOAPFault soapFault = getSoapFactory().createFault(reasonText, faultCode);
         soapFault.appendFaultSubcode(faultSubCode);
         return new SOAPFaultException(soapFault);
      } catch (SOAPException e) {
         throw new WebServiceException("Unable to create fault with SOAPFactory", e);
      }
   }

   private SOAPFactory getSoapFactory() throws SOAPException {
      return SOAPFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
   }
}
