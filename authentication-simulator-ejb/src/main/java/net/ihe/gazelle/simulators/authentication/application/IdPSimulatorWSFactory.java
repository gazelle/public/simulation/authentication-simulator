package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.preferences.PreferenceProvider;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;
import java.awt.image.Kernel;
import java.net.MalformedURLException;
import java.net.URL;

@Name("idPSimulatorWSFactory")
@AutoCreate
public class IdPSimulatorWSFactory implements IdPSimulatorFactory {

    private static final String IDP_NAMESPACE_URI = "http://ws.authentication.simulators.gazelle.ihe.net/";
    private static final String IDP_SERVICE_NAME = "IdPSimulatorService";

    private static final Logger LOG = LoggerFactory.getLogger(IdPSimulatorWSFactory.class);

    @In(value = "simulatorPreferenceProvider")
    private PreferenceProvider preferenceProvider;

    @Override
    public IdPSimulator getIdPSimulator() throws IdPConnectionException {
        try {
            QName qname = new QName(IDP_NAMESPACE_URI, IDP_SERVICE_NAME);
            Service service = Service.create(getIdPWsdlURL(), qname);
            return service.getPort(IdPSimulator.class);
        } catch (WebServiceException e) {
            LOG.error(e.getMessage());
            throw new IdPConnectionException("Unable to reach IdP Simulator Adapter endpoint", e);
        }
    }

    private URL getIdPWsdlURL() throws IdPConnectionException {
        try {
            String idpWsdlEndpoint = preferenceProvider.getString("idp_adapter_wsdl_endpoint");
            return new URL(idpWsdlEndpoint);
        } catch (MalformedURLException e) {
            throw new IdPConnectionException("Preference idp_adapter_wsdl_endpoint is wrongly defined", e);
        }
    }

}
