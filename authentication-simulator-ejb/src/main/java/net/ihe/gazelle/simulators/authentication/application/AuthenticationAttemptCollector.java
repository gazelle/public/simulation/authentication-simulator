package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Observer;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.async.QuartzDispatcher;
import org.jboss.seam.async.QuartzTriggerHandle;
import org.jboss.seam.async.TimerSchedule;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.xml.ws.WebServiceException;
import java.io.Serializable;
import java.util.List;

@Name("authenticationAttemptCollector")
@Scope(ScopeType.APPLICATION)
public class AuthenticationAttemptCollector implements Serializable {

    private static final long serialVersionUID = -8182027681655554053L;
    private static final Logger LOG = LoggerFactory.getLogger(AuthenticationAttemptCollector.class);

    private static final String JBOSS_SEAM_POST_INIT_EVENT = "org.jboss.seam.postInitialization";
    private static final String FETCH_LOG_EVENT = "net.ihe.gazelle.simulators.authentication.fetchLogEvent";
    private static final long DELAY = 10 * 1000L; //ms
    private static final long PERIOD = 3 * 60 * 1000L; //ms

    private QuartzTriggerHandle triggerHandle = null;
    private IdPSimulator idPSimulator = null;

    @In
    private EntityManager entityManager;

    @In
    private AuthenticationAttemptDAO authenticationAttemptDAO;

    @In(value = "idPSimulatorWSFactory")
    private IdPSimulatorFactory idPSimulatorFactory;

    public AuthenticationAttemptCollector() {
    }

    @Observer(value = JBOSS_SEAM_POST_INIT_EVENT)
    public void init() {
        triggerHandle = QuartzDispatcher.instance().scheduleTimedEvent(FETCH_LOG_EVENT, new TimerSchedule(DELAY, PERIOD));
        LOG.warn("Event {} scheduled every {} ms", FETCH_LOG_EVENT, PERIOD);
    }

    @Observer(FETCH_LOG_EVENT)
    @Transactional
    public synchronized void fetchIdPLogsEvent() {
        LOG.debug("start auto fetching idp logs");
        try {
            retrieveIdPLogs();
        } catch (IdPConnectionException e) {
            LOG.error("Error contacting IdP Simulator Adapter: {}", e.getMessage());
        } catch (IdpException e) {
            if (e.getPendingExceptions() != null && !e.getPendingExceptions().isEmpty()) {
                for (Exception idpE : e.getPendingExceptions()) {
                    LOG.error("Error reported by IdP Adapter", idpE);
                }
            } else {
                LOG.error(e.getMessage());
            }
        }
        LOG.debug("end auto fetching idp logs");
    }

    public synchronized void retrieveIdPLogs() throws IdPConnectionException, IdpException {
        List<AttemptRecordedByIdp> authenticationAttempts;
        try {
            authenticationAttempts = getIdPSimulator().fetchAuthenticationAttempts();
        } catch (WebServiceException e) {
            throw new IdPConnectionException("Connection with IdP broken", e);
        }
        for (AttemptRecordedByIdp authenticationAttempt : authenticationAttempts) {
            if (!authenticationAttemptDAO.isAttemptRecordedByIdpAlreadyPersisted(authenticationAttempt)) {
                authenticationAttempt = entityManager.merge(authenticationAttempt);
                LOG.debug("Authentication attempt {} persisted.", authenticationAttempt.getTimestamp());
            } else {
                LOG.debug("Authentication attempt {} already persisted, skipped.", authenticationAttempt.getTimestamp());
            }
        }
        entityManager.flush();
    }

    private IdPSimulator getIdPSimulator() throws IdPConnectionException {
        if (idPSimulator == null) {
            idPSimulator = idPSimulatorFactory.getIdPSimulator();
        }
        return idPSimulator;
    }

    @Destroy
    public void destroy() {
        if (triggerHandle != null) {
            try {
                triggerHandle.cancel();
                LOG.info("Event {} canceled", FETCH_LOG_EVENT);
            } catch (SchedulerException e) {
                LOG.error("Unable to cancel {} event schedule", FETCH_LOG_EVENT);
            }
        }
    }
}
