package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdpQuery;
import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedBySp;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.persistence.EntityManager;

@AutoCreate
@Name("authenticationAttemptDAO")
public class AuthenticationAttemptDAO {

    @In
    private EntityManager entityManager;

    public AttemptRecordedByIdp getAttemptRecordedByIdpById(int id) {
        return entityManager.find(AttemptRecordedByIdp.class, id);
    }

    public boolean isAttemptRecordedByIdpAlreadyPersisted(AttemptRecordedByIdp attemptRecordedByIdp) {
        AttemptRecordedByIdpQuery query = new AttemptRecordedByIdpQuery(entityManager);
        query.timestamp().eq(attemptRecordedByIdp.getTimestamp());
        query.authenticationSuccess().eq(attemptRecordedByIdp.getAuthenticationSuccess());
        query.username().eq(attemptRecordedByIdp.getUsername());
        query.usedBinding().eq(attemptRecordedByIdp.getUsedBinding());
        query.tokenId().eq(attemptRecordedByIdp.getTokenId());
        query.token().eq(attemptRecordedByIdp.getToken());
        query.serviceProviderName().eq(attemptRecordedByIdp.getServiceProviderName());
        query.identityProviderName().eq(attemptRecordedByIdp.getIdentityProviderName());
        AttemptRecordedByIdp persistedAttempt = query.getUniqueResult();
        return persistedAttempt != null;
    }

    public AttemptRecordedByIdpQuery getAttemptRecordedByIdpQuery() {
        return new AttemptRecordedByIdpQuery(entityManager);
    }

    public AttemptRecordedBySp getAttemptRecordedBySpById(int id) {
        return entityManager.find(AttemptRecordedBySp.class, id);
    }

}
