package net.ihe.gazelle.simulators.authentication.application;

public interface IdPSimulatorFactory {

    IdPSimulator getIdPSimulator() throws IdPConnectionException;

}
