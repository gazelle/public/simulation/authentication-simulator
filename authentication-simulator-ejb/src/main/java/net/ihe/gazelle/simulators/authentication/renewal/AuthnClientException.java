package net.ihe.gazelle.simulators.authentication.renewal;

public class AuthnClientException extends RuntimeException{

    public AuthnClientException() {
    }

    public AuthnClientException(String message) {
        super(message);
    }

    public AuthnClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthnClientException(Throwable cause) {
        super(cause);
    }
}
