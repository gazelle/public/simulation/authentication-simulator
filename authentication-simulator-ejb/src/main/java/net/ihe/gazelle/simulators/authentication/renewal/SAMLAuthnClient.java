package net.ihe.gazelle.simulators.authentication.renewal;

import net.ihe.gazelle.simulators.authentication.application.IdpException;
import org.opensaml.saml2.core.Assertion;

public interface SAMLAuthnClient {

    Assertion renew(String issuer, String username, String password);

}
