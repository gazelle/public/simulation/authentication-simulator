package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.simulators.authentication.model.AuthenticationAttempt;

public interface SPSimulator {

     AuthenticationAttempt startAuthentication();
}
