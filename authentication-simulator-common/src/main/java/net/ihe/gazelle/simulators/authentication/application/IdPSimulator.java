package net.ihe.gazelle.simulators.authentication.application;

import net.ihe.gazelle.simulators.authentication.model.AttemptRecordedByIdp;

import javax.ejb.Local;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.List;

@Local
@WebService(
        name = "IdPSimulatorWS",
        targetNamespace = "http://ws.authentication.simulators.gazelle.ihe.net/",
        serviceName = "IdPSimulatorService"
)
public interface IdPSimulator {

    /**
     * Get IdP SAML Metadata
     *
     * @return SAML Metadata file content in byte[]
     */
    @WebMethod
    @WebResult(name = "metadata")
    byte[] getIdPSimuMedatada() throws IdpException;

    /**
     * @return fetched last authentication attempts or empty list if nothing new
     */
    @WebMethod
    @WebResult(name = "authenticationAttempts")
    @XmlElementWrapper(name = "authenticationAttempts", required = true)
    List<AttemptRecordedByIdp> fetchAuthenticationAttempts() throws IdpException;

    /**
     * Reload metadata in IdP ($ ./reload-service.sh -id shibboleth.AttributeResolverService)
     */
    @WebMethod
    void reloadIdPMetadata() throws IdpException;

    /**
     * Reboot the IdP (sudo service tomcat8 restart)
     */
    @WebMethod
    void rebootIdp() throws IdpException;
}
