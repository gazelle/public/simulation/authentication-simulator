package net.ihe.gazelle.simulators.authentication.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.Objects;


@XmlRootElement(name = "attemptRecordedByIdp")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "attemptRecordedByIdpType", propOrder = {"serviceProviderName", "identityProviderName", "location"})
@Entity
@Table(name = "auth_attempt_recorded_by_idp", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "attemptSequence", sequenceName = "auth_attempt_recorded_by_idp_id_seq", allocationSize = 1)
public class AttemptRecordedByIdp extends AuthenticationAttempt {

    @XmlElement(name = "serviceProviderName")
    @Column(name = "service_provider_name")
    private String serviceProviderName;

    @XmlElement(name = "identityProviderName")
    @Column(name = "identity_provider_name")
    private String identityProviderName;

    @XmlElement(name = "location")
    @Enumerated(EnumType.STRING)
    @Column(name = "location")
    private LocationType location;

    public AttemptRecordedByIdp() {
    }

    public AttemptRecordedByIdp(Date timestamp, String username, String spName, String idpName, boolean success, BindingType usedBinding,
                                String tokenId, LocationType location) {
        super(timestamp, username, success, usedBinding, tokenId);
        this.serviceProviderName = spName;
        this.identityProviderName = idpName;
        this.location = location;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getIdentityProviderName() {
        return identityProviderName;
    }

    public void setIdentityProviderName(String identityProviderName) {
        this.identityProviderName = identityProviderName;
    }

    public LocationType getLocation() {
        return location;
    }

    public void setLocation(LocationType location) {
        this.location = location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        AttemptRecordedByIdp that = (AttemptRecordedByIdp) o;
        return Objects.equals(serviceProviderName, that.serviceProviderName) &&
                Objects.equals(identityProviderName, that.identityProviderName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), serviceProviderName, identityProviderName);
    }
}
