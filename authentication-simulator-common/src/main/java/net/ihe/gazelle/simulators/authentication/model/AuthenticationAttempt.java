package net.ihe.gazelle.simulators.authentication.model;

import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import java.util.Date;
import java.util.Objects;

@XmlType(name = "authenticationAttemptType", propOrder = {"username", "usedBinding", "tokenId", "token"})
@MappedSuperclass
public abstract class AuthenticationAttempt {

    @XmlTransient
    @Id
    @GeneratedValue(generator = "attemptSequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @XmlAttribute(name = "timestamp", required = true)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "timestamp")
    private Date timestamp;

    @XmlAttribute(name = "success", required = true)
    @Column(name = "authentication_success")
    private Boolean authenticationSuccess;

    @XmlElement(name = "username")
    @Column(name = "username")
    private String username;

    @XmlElement(name = "usedBinding")
    @Enumerated(EnumType.STRING)
    @Column(name = "used_binding")
    private BindingType usedBinding;

    @XmlElement(name = "tokenId")
    @Column(name = "token_id")
    private String tokenId;

    @XmlElement(name = "token")
    @Lob
    @Type(type = "text")
    @Column(name = "token")
    private String token;

    public AuthenticationAttempt() {
    }

    public AuthenticationAttempt(Date timestamp, String username, boolean success, BindingType usedBinding,
                                 String tokenId) {

        this.timestamp = (Date) timestamp.clone();
        this.username = username;
        this.authenticationSuccess = success;
        this.usedBinding = usedBinding;
        this.tokenId = tokenId;
    }

    public AuthenticationAttempt(Date timestamp, String username, boolean success, BindingType usedBinding,
                                 String tokenId, String token) {

        this.timestamp = (Date) timestamp.clone();
        this.username = username;
        this.authenticationSuccess = success;
        this.usedBinding = usedBinding;
        this.tokenId = tokenId;
        this.token = token;
    }

    public Date getTimestamp() {
        return (Date) timestamp.clone();
    }

    public String getUsername() {
        return username;
    }

    public Boolean isAuthenticationSuccess() {
        return authenticationSuccess;
    }

    public Boolean getAuthenticationSuccess() {
        return authenticationSuccess;
    }

    public BindingType getUsedBinding() {
        return usedBinding;
    }

    public String getTokenId() {
        return tokenId;
    }

    public String getToken() {
        return token;
    }

    public Integer getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AuthenticationAttempt that = (AuthenticationAttempt) o;
        return Objects.equals(timestamp, that.timestamp) &&
                Objects.equals(username, that.username) &&
                Objects.equals(authenticationSuccess, that.authenticationSuccess) &&
                usedBinding == that.usedBinding &&
                Objects.equals(tokenId, that.tokenId) &&
                Objects.equals(token, that.token);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, username, authenticationSuccess, usedBinding, tokenId, token);
    }
}
