package net.ihe.gazelle.simulators.authentication.model;

public enum BindingType {

    HTTP_REDIRECT_BINDING("urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect"),
    SOAP_BINDING("urn:oasis:names:tc:SAML:2.0:bindings:SOAP"),
    HTTP_POST_BINDING("urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"),
    HTTP_POST_BINDING_SIMPLE_SIGN("urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST-SimpleSign");

    private String label;

    BindingType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static BindingType getBindingTypeByLabel(String label) {
        for (BindingType bt : values()) {
            if (bt.getLabel().equals(label)) {
                return bt;
            }
        }
        return null;
    }
}
