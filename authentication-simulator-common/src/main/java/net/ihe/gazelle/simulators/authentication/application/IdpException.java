package net.ihe.gazelle.simulators.authentication.application;

import java.util.List;

public class IdpException extends Exception {

    private static final long serialVersionUID = 4887130425088786635L;
    private List<Exception> pendingExceptions;

    public IdpException() {
        super();
    }

    public IdpException(String s) {
        super(s);
    }

    public IdpException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public IdpException(Throwable throwable) {
        super(throwable);
    }

    protected IdpException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }

    public IdpException(String s, List<Exception> pendingExceptions) {
        super(s);
        this.pendingExceptions = pendingExceptions;
    }

    public List<Exception> getPendingExceptions() {
        return pendingExceptions;
    }

}
