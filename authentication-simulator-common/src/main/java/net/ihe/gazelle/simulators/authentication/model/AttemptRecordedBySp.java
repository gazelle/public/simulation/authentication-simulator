package net.ihe.gazelle.simulators.authentication.model;

import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Date;

@Entity
@Table(name = "auth_attempt_recorded_by_sp", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
@SequenceGenerator(name = "attemptSequence", sequenceName = "auth_attempt_recorded_by_sp_id_seq", allocationSize = 1)
public class AttemptRecordedBySp extends AuthenticationAttempt{

    public AttemptRecordedBySp(){
    }

    public AttemptRecordedBySp(Date timestamp, String username, boolean success, BindingType usedBinding,
                               String tokenId, String token){
        super(timestamp, username, success, usedBinding, tokenId, token);
    }
}
