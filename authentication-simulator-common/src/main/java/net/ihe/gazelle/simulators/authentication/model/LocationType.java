package net.ihe.gazelle.simulators.authentication.model;

public enum LocationType {

    REDIRECT_SSO("/idp/profile/SAML2/Redirect/SSO"),
    SOAP_ECP("/idp/profile/SAML2/SOAP/ECP"),
    ARTIFACT_RESOLUTION("/idp/profile/SAML2/SOAP/ArtifactResolution"),
    HTTP_POST_SSO("/idp/profile/SAML2/POST/SSO"),
    HTTP_POST_SIMPLE_SIGN_SSO("/idp/profile/SAML2/POST-SimpleSign/SSO");

    private String label;

    LocationType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public static LocationType getLocationTypeByUrl(String url) {
        for (LocationType lt : values()) {
            if (url.contains(lt.getLabel())) {
                return lt;
            }
        }
        return null;
    }
}
